import unittest
from grid_code_01 import gridChallenge
class GridChalenge(unittest.TestCase):
    def test_gridChalenge(self):
        grid = ['abcde','efghi','ijklm','mnopq','stuvw']
        answer = 'YES'
        self.assertEqual(gridChallenge(grid),answer)
    
    def test_gridChalenge_1(self):
        grid = ['edcba','dcbae','ijklm','cbaed']
        answer = 'NO'
        self.assertEqual(gridChallenge(grid),answer)

if __name__ == '__main__':
	unittest.main()