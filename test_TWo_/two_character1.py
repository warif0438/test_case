#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'alternate' function below.
#
# The function is expected to return an INTEGER.
# The function accepts STRING s as parameter.
#


def alternate(s):
    from itertools import combinations
    max_len = 0
    for c1, c2 in combinations(set(s), 2):
        temp = ''.join(ch for ch in s if ch == c1 or ch == c2)
        valid = True
        for i in range(len(temp) - 1):
            if temp[i] == temp[i + 1]:
                valid = False
                break
        if valid:
            max_len = max(max_len, len(temp))
    return max_len


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    l = int(input().strip())

    s = input()

    result = alternate(s)

    fptr.write(str(result) + '\n')

    fptr.close()
