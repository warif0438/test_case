import unittest
from fizz_buzz_code import fizzbuzz

# class TestFizzBuzz(unittest.TestCase):
#     def test_give_15_30_60_should_Fizz_Buzz(self):
#         x = 15
#         x = 30
#         x = 60
#         expected_result = "FizzBuzz"
#         result = Fizz-Buzz.Fizz_Buzz(x,x,x)
#         self.assertEqual(expected_result, result)


class TestFizzBuzz(unittest.TestCase):

	def test_simple_should_return_the_number(self):
		self.assertEqual(fizzbuzz(1), "NonFizzBuzz")
		self.assertEqual(fizzbuzz(2), "NonFizzBuzz")
		self.assertEqual(fizzbuzz(4), "NonFizzBuzz")

	def test_multiple_3_should_return_Fizz(self):
		self.assertEqual(fizzbuzz(3), "Fizz")
		self.assertEqual(fizzbuzz(9), "Fizz")

	def test_multiple_5_should_return_Buzz(self):
		self.assertEqual(fizzbuzz(5), "Buzz")
		self.assertEqual(fizzbuzz(10), "Buzz")
	
	def test_multiple_3_and_5_should_return_FizzBuzz(self):
		self.assertEqual(fizzbuzz(15), "FizzBuzz")
		self.assertEqual(fizzbuzz(30), "FizzBuzz")
		

if __name__ == '__main__':
	unittest.main()