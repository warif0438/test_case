import unittest
from funnystring import funnyString

class TestFunnyString(unittest.TestCase):
    def test_funny_string_return_funny_1(self):
       self.assertEqual(funnyString('2'), "Funny")
    def test_funny_string_return_funny_11(self):
           self.assertEqual(funnyString(' '), "Funny")
        
    def test_funny_string_return_funny_2(self):
        self.assertEqual(funnyString('ovyvzvptyvpvpxyztlrztsrztztqvrxtxuxq'), "Funny")

    def test_funny_string_return_funny_3(self):
        self.assertEqual(funnyString('10'), "Funny")
    def test_funny_string_return_funny_4(self):
        self.assertEqual(funnyString('tmruzxzuwoskqysxztuvosuyrswrnmtxvzsrqwytzrxpltrwusxupw'), "Funny")
        
    def test_funny_string_return_not_funny_1(self):
        self.assertEqual(funnyString('eklrywzvpxtvoptlrskmskszvwzsuzxrtvyzwruqvyxusqwupnurqmtltnltsmuyxqoksyurpwqpv'), "Not Funny")
        
    def test_funny_string_return_not_funny_2(self):
        self.assertEqual(funnyString('fmpszyvqwxrtvpuwqszvyvotmsxsxuvzyvpwzrpmuxqwtswvytytzsnuxuyrpvtysqoutzurqxury'), "Not Funny")
        
    

if __name__ == '__main__':
	unittest.main()
        