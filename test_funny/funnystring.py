#!/bin/python3

import math
import os
import random
import re
import sys

#
# Complete the 'funnyString' function below.
#
# The function is expected to return a STRING.
# The function accepts STRING s as parameter.
#

def funnyString(s):
    r = s[::-1]
    for i in range(len(s)-1):
        a = ord(s[i])
        b = ord(s[i+1])
        c = ord(r[i])
        d = ord(r[i+1])
        if abs(a - b) != abs(c - d):
            return 'Not Funny'
    return 'Funny'
    
if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    q = int(input().strip())

    for q_itr in range(q):
        s = input()

        result = funnyString(s)

        fptr.write(result + '\n')

    fptr.close()
