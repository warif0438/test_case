import unittest
from caesar_Cipher_code import caesarCipher

class CaesarCipher(unittest.TestCase):
    def test_give_4_Hello_World_should_Lipps_Asvph(self):
        str_encode = "Hello_World!"
        input_longest_str = 4
        expected_result = "Lipps_Asvph!"
        self.assertEqual(caesarCipher(str_encode,input_longest_str),expected_result)
    def test_give_5_AlwaysLookontheBrightSideofLife_should_FqbfdxQttptsymjGwnlmyXnijtkQnkj(self):
        str_encode = "Always-Look-on-the-Bright-Side-of-Life"
        input_longest_str = 5
        expected_result = "Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj"
        self.assertEqual(caesarCipher(str_encode,input_longest_str),expected_result)
    def test_give_27_1X7T4VrCs23k4vv08D6yQ3S19G4rVP188M9ahuxB6j1tMGZs1m10ey7eUj62WV2exLT4C83zl7Q80M_should_FqbfdxQttptsymjGwnlmyXnijtkQnkj(self):
        str_encode = "1X7T4VrCs23k4vv08D6yQ3S19G4rVP188M9ahuxB6j1tMGZs1m10ey7eUj62WV2exLT4C83zl7Q80M"
        input_longest_str = 27
        expected_result = "1Y7U4WsDt23l4ww08E6zR3T19H4sWQ188N9bivyC6k1uNHAt1n10fz7fVk62XW2fyMU4D83am7R80N"
        self.assertEqual(caesarCipher(str_encode,input_longest_str),expected_result)
    def test_give_66_aa_should_ll(self):
        str_encode = "Pz-/aI/J`EvfthGH"
        input_longest_str = 66
        expected_result = "Dn-/oW/X`SjthvUV"
        self.assertEqual(caesarCipher(str_encode,input_longest_str),expected_result)
    
if __name__ == '__main__':
	unittest.main()